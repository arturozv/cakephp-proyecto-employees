<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Title[]|\Cake\Collection\CollectionInterface $titles
 */
?>
<div class="titles index content">
    <?= $this->Html->link(__('New Title'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Titles') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('emp_no','No. Empleado') ?></th>
                    <th><?= $this->Paginator->sort('title','Titulo') ?></th>
                    <th><?= $this->Paginator->sort('from_date','Fecha Inicio') ?></th>
                    <th><?= $this->Paginator->sort('to_date','Fecha Fin') ?></th>
                    <th class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($titles as $title): ?>
                <tr>
                    <td><?= $this->Html->link($this->Number->format($title->emp_no), ['controller'=> 'employees', 'action' => 'view', $title->emp_no]) ?></td>
                    <td><?= h($title->title) ?></td>
                    <td><?= h($title->from_date) ?></td>
                    <td><?= h($title->to_date) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $title->emp_no, $title->title, $title->from_date->format('Y-m-d')]) //Se agregan las dos llaves adicionales, es decir el titulo y la fecha inicio en view y edit ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $title->emp_no, $title->title, $title->from_date->format('Y-m-d')]) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $title->emp_no], ['confirm' => __('¿Estás seguro de eliminar el registro # {0}?', $title->emp_no)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primero')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Ultimo') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
