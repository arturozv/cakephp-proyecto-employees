<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.10.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->disableAutoLayout();

if (!Configure::read('debug')) :
    throw new NotFoundException(
        'Please replace templates/Pages/home.php with your own version or re-enable debug mode.'
    );
endif;
?>

<?php
$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <?= $this->Html->css(['normalize.min', 'milligram.min', 'cake', 'home']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

<div class="container text-center">
            <a href="https://cakephp.org/" target="_blank" rel="noopener">
                <img alt="CakePHP" src="https://cakephp.org/v2/img/logos/CakePHP_Logo.svg" width="350" />
            </a>
            <h1>
                Módulos 🍓
            </h1>
        </div>


<!-- MENU PRINCIPAL CON TODOS LOS MODULOS DEL SISTEMA !-->
<main class="main">
        <div class="container">
            <div class="content">
                <div class="row">
                    <div class="column">
                        <?= $this->Html->link(__('Empleados'), ['controller'=> 'employees'], ['action' => 'index'], ['class' => 'button']) ?>
                        <br><br>
                        <?= $this->Html->link(__('Titulos'), ['controller'=> 'titles'], ['action' => 'index'], ['class' => 'button']) ?>
                        <br><br>
                        <?= $this->Html->link(__('Salarios'), ['controller'=> 'salaries'], ['action' => 'index'], ['class' => 'button']) ?>
                        <br><br>
                        <?= $this->Html->link(__('Departamentos'), ['controller'=> 'departments'], ['action' => 'index'], ['class' => 'button']) ?>
                        <br><br>
                        <?= $this->Html->link(__('DeptEmp'), ['controller'=> 'deptEmp'], ['action' => 'index'], ['class' => 'button']) ?>
                        <br><br>
                        <?= $this->Html->link(__('DeptManager'), ['controller'=> 'deptManager'], ['action' => 'index'], ['class' => 'button']) ?>
                        <br><br>
                    </div>
                </div>
            </div>
        </div>
</main>

</body>


</html>
