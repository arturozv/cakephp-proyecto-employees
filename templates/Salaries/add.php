<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Salary $salary
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Salaries'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="salaries form content">
            <?= $this->Form->create($salary) ?>
            <fieldset>
                <legend><?= __('Añadir Salario') ?></legend>
                <?php
                //Se agregan los datos faltantes , asi como las reglas para las fechas.
                    echo $this->Form->control('emp_no', ['label' => 'No. empleado', 'type' => 'number']);
                    echo $this->Form->control('salary', ['label' => 'Salario']);
                    echo $this->Form->control('from_date', [
                        'label' => 'Fecha inicio',
                        'type' => 'date',
                        'empty' => true,
                        'minYear' => date('Y') - 50,
                        'maxYear' => date('Y') + 50,
                        'monthNames' => false
                        ]);
                    echo $this->Form->control('to_date', [
                        'label' => 'Fecha fin',
                        'empty' => true,
                        'minYear' => date('Y') - 50,
                        'maxYear' => date('Y') + 50,
                        'monthNames' => false
                        ]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
