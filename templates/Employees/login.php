<div class="employees form large-9 medium-8 columns content">
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Login') ?></legend>
        <?php
            echo $this->Form->control('email');
            echo $this->Form->control('password');
        ?>
    </fieldset>
    <?php
    echo $this->Form->button('login',['class'=>'btn btn-primary'],['action'=>'index']);
    echo $this->Html->link('Olvidé mi contraseña',['action'=>'forgotpassword'],['class'=>'btn btn-info']);
    echo $this->Form->end();
    ?>
</div>