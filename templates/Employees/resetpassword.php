<div class="row">
    <div class="col-md-4 offset-md-4">
        <?php echo $this->Flash->render() ?>
        <div class="card">
            <h3 class="card-header">Cambio de contraseña</h3>
            <div class="card-body">
                <?php echo  $this->Form->create() ?>
                <div class="form-group">
                    <?php echo $this->Form->input('password', ['class'=>'form-control']) ?>
                </div>
                    <?php
                    //A este modulo se accede mediante un enlace que se envía al correo
                        echo $this->Form->button('Cambia tu contraseña',['class'=>'btn btn-primary']);
                        echo $this->Form->end(); 
                    ?>
            </div>
        </div>
    </div>
</div>