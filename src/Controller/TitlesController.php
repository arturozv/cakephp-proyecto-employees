<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Titles Controller
 *
 * @property \App\Model\Table\TitlesTable $Titles
 * @method \App\Model\Entity\Title[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TitlesController extends AppController
{


    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $titles = $this->paginate($this->Titles);

        $this->set(compact('titles'));
    }

    /**
     * View method
     *
     * @param string|null $id Title id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $title = null, $from_date = null)
    {
        $title = $this->Titles->get([$id, $title, $from_date]);

        $this->set('title', $title);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $title = $this->Titles->newEmptyEntity();
        if ($this->request->is('post')) {
            $title = $this->Titles->patchEntity($title, $this->request->getData());
            if ($this->Titles->save($title)) {
                $this->Flash->success(__('The title has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The title could not be saved. Please, try again.'));
        }
        $this->set(compact('title'));
    }
    

    /**
     * Edit method
     *
     * @param string|null $id Title id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $title = null, $from_date = null)
    {
        $title = $this->Titles->get([$id, $title, $from_date]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $title = $this->Titles->patchEntity($title, $this->request->getData());
            if ($this->Titles->save($title)) {
                $this->Flash->success(__('The title has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The title could not be saved. Please, try again.'));
        }
        $this->set(compact('title'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Title id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $title = null, $from_date = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $title = $this->Titles->get([$id, $title, $from_date]);
        if ($this->Titles->delete($title)) {
            $this->Flash->success(__('The title has been deleted.'));
        } else {
            $this->Flash->error(__('The title could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);

    }


/**
     * Método para ver todos los registros de los titulos en donde el empleado del titulo en mujer
     *
     * @return \cake\Http\Response|null
     */
    public function listaMujeres()
    {
        $query = $this->Titles->find()
            ->contain('Employees')
            ->where(['Employees.gender' => 'M']);
        $titulosMujeres = $this->paginate($titles);
        $this->set(compact('titulosMujeres'));
        //sql($query);

        //debug($query->enableHydration(false)->toList());
       // exit;
    }
}