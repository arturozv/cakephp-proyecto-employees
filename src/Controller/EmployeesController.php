<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Employees Controller
 *
 * @property \App\Model\Table\EmployeesTable $Employees
 * @method \App\Model\Entity\Employee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmployeesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $employees = $this->paginate($this->Employees);

        $this->set(compact('employees'));
    }

    /**
     * View method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('employee'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $employee = $this->Employees->newEmptyEntity();
        if ($this->request->is('post')) {
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('The employee has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The employee could not be saved. Please, try again.'));
        }
        $this->set(compact('employee'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('The employee has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The employee could not be saved. Please, try again.'));
        }
        $this->set(compact('employee'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $employee = $this->Employees->get($id);
        if ($this->Employees->delete($employee)) {
            $this->Flash->success(__('The employee has been deleted.'));
        } else {
            $this->Flash->error(__('The employee could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    
//Método de inicio de sesion
    public function login()
    {
        if($this->request->is('post')) {
            $employee = $this->Auth->identify();
            if($employee) {
                $this->Auth->setUser($employee);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }
// Método para cerrar sesion
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function forgotpassword()
    {
        //Método para solicitar el enlace que será enviado al correo del empleado
        //No sirve esta funcion, no pude encontrar el motivo.
        if($this->request->is['post']){
            $myemail = $this->request->getData('email');
            $mytoken = Security::hash(Security::randomBytes(25));

            $employeeTable = EmployeesTable::get('Employees');
            $employee = $employeeTable->find('all')->where(['email'=>$myemail])->first();
            $employee->password = '';
            $employee->token = $mytoken;
            if ($employeeTable->save($employee)) {
                $this->Flash->success('El enlace de cambio de contraseña ha sido enviado a tu correo ('.$myemail.').');
                    //Estos datos se obtienen de mailtrap, el cual es un sitio para hacer pruebas.
                Email::configTransport('mailtrap', [
                    'host' => 'smtp.mailtrap.io',
                    'port' => 2525,
                    'username' => 'fdc0bafec0397d',
                    'password' => '52e5eea8317098',
                    'className' => 'Smtp'
                  ]);
                    //El cuerpo del correo que será enviado al empleado
                  $email = new Email('default');
                  $email->transport('mailtrap');
                  $email->emailFormat('html');
                  $email->from('arturo11w@gmail.com','Arturo Zavala');
                  $email->subject('Por favor, confirma tu cambio de contraseña.');
                  $email->to($myemail);
                  $email->send('Hello '.$myemail.'<br/>Por favor dale click al enlace que se encuentra abajo para reiniciar tu contraseña.<br/><br/><a href="http://localhost:8765/resetpassword/'.$mytoken.'">Reset Password</a>');
            }

        }
    }
    
    public function resetpassword()
    {
        //Función para cambiar la contraseña
        if($this->request->is('post')){
            $hasher = new DefaultPasswordHasher();
            $mypass = $hasher->hash($this->request->getData('password'));

            $employeeTable = EmployeesTable::get['Employees'];
            $employee = $employeeTable->find('all')->where(['token'=>$token])->first();
            $employee->password = $mypass;
            if($employeeTable->save($employee)){
                return $this->redirect(['action'=>'login']);
            }
        }
    }

}
