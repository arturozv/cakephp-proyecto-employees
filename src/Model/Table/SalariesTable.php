<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Salaries Model
 *
 * @method \App\Model\Entity\Salary newEmptyEntity()
 * @method \App\Model\Entity\Salary newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Salary[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Salary get($primaryKey, $options = [])
 * @method \App\Model\Entity\Salary findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Salary patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Salary[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Salary|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Salary saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Salary[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Salary[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Salary[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Salary[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SalariesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('salaries');
        $this->setDisplayField('emp_no');
        $this->setPrimaryKey(['emp_no', 'from_date']);

        //Asociaciones
        $this->belongsTo('Employees')
            ->setForeignKey('emp_no');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
        // Se establece como int y obligatoria.
            ->integer('emp_no')
            ->requirePresence('emp_no')
            ->notEmpty('emp_no');

        $validator
        // el salario debe ser int, y debe haber siempre un salario.
            ->integer('salary')
            ->requirePresence('salary')
            ->notEmpty('salary');

        $validator
        // se le agrega formato de fecha y se fija como obligatoria
            ->date('from_date')
            ->requirePresence('from_date')
            ->notEmptyDate('from_date');

        $validator
        // se agrega formato de fecha y es un valor opcional.
            ->date('to_date')
            ->allowEmptyDate('to_date');

        return $validator;
    }
// Esta regla establece que si se agrega un empleado que no existe no se puede agregar el salario.
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add(
            $rules->existsIn(
                ['emp_no'],
                'Employees',
                'Llave foránea no encontrada'
            )
        );

        return $rules;
    }
}
